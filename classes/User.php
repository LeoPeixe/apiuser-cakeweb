<?php 
include_once 'cdb.php';

class User {
	public function __construct() {
		$this->data = '';
		$this->model = new CDB;
	}
	public function Get($id = null) {
		$where = 'deletado = ?';
		$values = array(0);
		if ($id != null) {
			$where .= ' AND id = ?';
			$values[] = $id;
		}
		$user = $this->model->Query('SELECT * FROM usuarios WHERE ' . $where, $values);
		foreach ($user as $key => $value) {
			$uploaddir = 'uploads/' . $value['id'] . '.jpg';

			if (file_exists($uploaddir)) {
				$file = $value['id'] . '.jpg';
				$user[$key]['avatar'] = $file;
			} else {
				$user[$key]['avatar'] = null;
			}
		}
		return $user;
	}
	public function Insert($post) {
		$names = '';
		$pdo_scp = '';
		$values = array();

		foreach ($post as $key => $value) {
			$names .= ', ' . $key;
			$pdo_scp .= ', ?';
			if ($key == 'senha') {
				$value = hash('sha256', $value);
			}
			if ($key == 'email') {
				if (!$this->model->checkEmail($value)) {
					return "E-mail incorrect";
				}
			}
			$values[] = $value;
		}

		$names = substr($names, 1);
		$pdo_scp = substr($pdo_scp, 1);

		$insert = $this->model->Query('INSERT INTO usuarios 
									(' . $names . ')
									VALUES (' . $pdo_scp . ')', 
									$values, 'lastid');
		return $insert;
	}
	public function Update($post, $id) {
		$names = '';
		$values = array();

		foreach ($post[0] as $key => $value) {
			$names .= ', ' . $key . ' = ?';
			if ($key == 'senha') {
				$value = hash('sha256', $value);
			}
			if ($key == 'email') {
				if (!$this->model->checkEmail($value)) {
					return "E-mail incorrect";
				}
			}
			$values[] = $value;
		}
		$values[] = $id;
		$values[] = 0;

		$names = substr($names, 1);

		$user = $this->model->Query('UPDATE usuarios SET ' . $names . ' WHERE id = ? AND deletado = ?', $values, 'update');
		if ($user[0] != '00000') {
			return $user[2];
		} else {
			return true;
		}
	}
	public function Delete($id) {
		$values = array();
		$values[] = 1;
		$values[] = $id;
		$values[] = 0;

		$user = $this->model->Query('UPDATE usuarios SET deletado = ? WHERE id = ? AND deletado = ?', $values, 'delete');
		if ($user[0] != '00000') {
			return $user[2];
		} else {
			return true;
		}
	}
}