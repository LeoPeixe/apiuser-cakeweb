<?php
#Custom Data Base Connect Class PDO Connection 
class CDB {

	public function __construct() {
		$dsn = 'mysql:host=localhost;dbname=apiuser';
		$user = 'root';
		$pass = '';

		try {
	    $this->pdo = new PDO($dsn, $user, $pass);
		} catch (PDOException $e) {
	    print "Error!: " . $e->getMessage() . "<br/>";
	    die();
		}
	}
	// $query,$where = null, array $values = null
	public function Query($query, array $value = null,$tipo = null) {
			$sth = $this->pdo->prepare($query);
			if (count($value) > 0) {
				$i = 1;
				foreach ($value as $key => $val) {
	        switch ($val) {
	            case is_int($val):
	                $type = PDO::PARAM_INT;
	                break;
	            case is_bool($val):
	                $type = PDO::PARAM_BOOL;
	                break;
	            case is_null($val):
	                $type = PDO::PARAM_NULL;
	                break;
	            default:
	                $type = PDO::PARAM_STR;
	        }
			    $sth->bindValue($i, $val, $type);
			    $i += 1;
				}
			}
			$sth->execute();
	    if ($tipo == 'lastid') {
		    return $this->pdo->lastInsertId();
	    } else if($tipo != null) {
	    	return $sth->errorInfo();
	    } else {
		    $result = $sth->fetchAll();
		    return $this->clean($result);
	    }
	}
	public function checkEmail($email) {
	   	if ( strpos($email, '@') !== false ) {
			$split = explode('@', $email);
			return (strpos($split['1'], '.') !== false ? true : false);
	   	} else {
			return false;
		}
	}
	private function clean($result) {
		foreach ($result as $key => $value) {
			foreach ($value as $key2 => $value2) {
				if (is_int($key2)) {
					unset($result[$key][$key2]);
				}
			}
		}
		return $result;
	}
}