<?php
header("Access-Control-Allow-Origin: *");

require 'vendor/autoload.php';

require_once 'classes/User.php';

# JSON USERS #
Flight::route('GET /usuarios', function(){
	$user = new User;
	$result = $user->Get();

	Flight::json($result);
});

# JSON USER BY ID #
Flight::route('GET /usuarios/@id', function($id){
	$user = new User;
	$result = $user->Get($id);

	Flight::json($result);
});

# CREATE USER #
Flight::route('POST /usuarios', function(){
	$data = Flight::request()->data;
	$file = Flight::request()->files->avatar;
	$ext = pathinfo($file['name'], PATHINFO_EXTENSION);

	if (count($file) > 0 && $ext != 'jpg' && $ext != 'jpeg' && $ext != 'png') {
		Flight::json(false);
		die();
	}

	$user = new User;
	$result = $user->Insert($data);

	if (count($file) > 0 && $result > 0) {
		$uploaddir = 'uploads/';
		$uploadfile = $uploaddir . $result . '.jpg';

		move_uploaded_file($file['tmp_name'], $uploadfile);
	}


	// Retorna "0" em caso de falha ou o id do cadastro, em caso de sucesso
	Flight::json($result);
});

# UPDATE NEW AVATAR USER #
Flight::route('POST /usuarios/fileupload/@id', function($id){
	$file = Flight::request()->files->avatar;
	$ext = pathinfo($file['name'], PATHINFO_EXTENSION);

	if (count($file) > 0 && $ext != 'jpg' && $ext != 'jpeg' && $ext != 'png') {
		Flight::json(false);
		die();
	}

	if (count($file) > 0 && $id > 0) {
		$uploaddir = 'uploads/' . $id . '.jpg';
		
		if (file_exists($uploaddir)) {
			unlink($uploaddir);
		}

		$uploadfile = $uploaddir;

		$result = move_uploaded_file($file['tmp_name'], $uploadfile);
	}


	// Retorna true ou false
	Flight::json($result);
});

# UPDATE USER #
Flight::route('PUT /usuarios/@id', function($id){
	$data = Flight::request()->getBody();
	$data = json_decode($data, true);

	$user = new User;
	$result = $user->Update($data, $id);

	// Retorna o erro, caso algo tenha dado errado.
	Flight::json($result);
});

# DELETE USER #
Flight::route('DELETE /usuarios/@id', function($id){
	$user = new User;
	$result = $user->Delete($id);

	// Retorna o erro, caso algo tenha dado errado.
	Flight::json($result);
});

Flight::start();
